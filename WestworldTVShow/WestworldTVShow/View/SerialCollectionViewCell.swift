//
//  SerialCollectionViewCell.swift
//  WestworldTVShow
//
//  Created by Лидия Некрасова on 18.05.2023.
//

import UIKit

class SerialCollectionViewCell: UICollectionViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet var serialNameLabel: UILabel!
    @IBOutlet var serialDescriptionLabel: UILabel!
    @IBOutlet var serialImage: ImagesImageView!
    
    @IBOutlet var ratingLabel: UILabel!
    @IBOutlet var ratingImageView: UIImageView!
    
    // MARK: - Public methods
    func configure(with serial: WestWorld) {
        
        serialNameLabel.text = serial.name
        serialDescriptionLabel.text = serial.description
        serialImage.fetchImage(from: serial.imageSerial.medium ?? "")
        
        ratingLabel.text = String(serial.rating.average)
        ratingImageView.image = #imageLiteral(resourceName: "Star")
    }
}
