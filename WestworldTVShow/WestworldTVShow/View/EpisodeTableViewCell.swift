//
//  EpisodeTableViewCell.swift
//  WestworldTVShow
//
//  Created by Лидия Некрасова on 19.05.2023.
//

import UIKit

class EpisodeTableViewCell: UITableViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet var episodeNameLabel: UILabel!
    @IBOutlet var episodeDescriptionLabel: UILabel!
    @IBOutlet var episodeImageView: ImagesImageView!
    
    // MARK: - Public methods
    func configure(with episode: Episode) {
        episodeNameLabel.text = episode.name
        episodeDescriptionLabel.text = episode.description
        episodeImageView.fetchImage(from: episode.image.medium ?? "")
    }
}
