//
//  ImagesImageView.swift
//  WestworldTVShow
//
//  Created by Лидия Некрасова on 23.05.2023.
//

import UIKit

class ImagesImageView: UIImageView {
    
    // MARK: - Public properties
    private let activityIndicator = UIActivityIndicatorView(style: .medium)
    
    // MARK: - Public methods
    func fetchImage(from url: String) {
        
        activityIndicator(false)
        
        guard let imageURL = URL(string: url) else {
            image = #imageLiteral(resourceName: "defaultPicture")
            return
        }
        
        /// Используем изображение из кеша, если есть
        if let cahcedImage = getCachedImage(from: imageURL) {
            self.activityIndicator(true)
            image = cahcedImage
            return
        }
        
        /// Если изображения в кеше нет, то гризим его из сети
        ImageNetworkManager.shared.fetchImage(from: imageURL) { (data, response) in
            DispatchQueue.main.async {
                self.activityIndicator(true)
                self.image = UIImage(data: data)
            }
            /// Сохраняем изображение в кеш
            self.saveDataToCache(with: data, and: response)
        }
    }
    
    // MARK: - Private methods
    ///Работа с activityIndicator
    private func activityIndicator(_ isHidden: Bool) {
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(activityIndicator)
        
        activityIndicator.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        activityIndicator.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        
        if !isHidden {
            activityIndicator.startAnimating()
            activityIndicator.hidesWhenStopped = true
        } else {
            activityIndicator.stopAnimating()
        }
    }
    
    ///Получаем изображение из кеша
    private func getCachedImage(from url: URL) -> UIImage? {
        let urlRequest = URLRequest(url: url)
        if let cachedResponse = URLCache.shared.cachedResponse(for: urlRequest) {
            return UIImage(data: cachedResponse.data)
        }
        return nil
    }
    
    /// Сохранение изображения в кеш
    private func saveDataToCache(with data: Data, and reponse: URLResponse) {
        guard let urlResponse = reponse.url else { return }
        let urlRequest = URLRequest(url: urlResponse)
        let cachedResponse = CachedURLResponse(response: reponse, data: data)
        URLCache.shared.storeCachedResponse(cachedResponse, for: urlRequest)
    }
}

