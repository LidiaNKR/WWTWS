//
//  ImageNetworkManager.swift
//  WestworldTVShow
//
//  Created by Лидия Некрасова on 18.05.2023.
//

import Foundation

class ImageNetworkManager {
    
    //Класс является синглтоном
    static var shared = ImageNetworkManager()
    
    private init() {}
    
    ///Парсим JSON - image
    func fetchImage(from url: URL, completion: @escaping (Data, URLResponse) -> Void) {
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            guard let data = data, let response = response else {
                print(error?.localizedDescription ?? "No error description")
                return
            }
            
            guard url == response.url else { return }
            
            completion(data, response)
            
        }.resume()
    }
}
