//
//  NetworkManager.swift
//  WestworldTVShow
//
//  Created by Лидия Некрасова on 18.05.2023.
//

import Foundation
import UIKit

class NetworkManager {
    
    //Класс является синглтоном
    static let shared = NetworkManager()
    
    private init() {}
    
    ///Парсим JSON
    func fetchData(from url: String?, with completon: @escaping (WestWorld) -> Void) {
        //извлекаем опционал
        guard let stringURL = url else { return }
        //инициализируем URL-адрес
        guard let url = URL(string: stringURL) else { return }
        
        //Создаем сессию запроса данных
        URLSession.shared.dataTask(with: url) { (data, _, error) in
            if let error = error {
                print(error)
                return
            }
            
            //Извлекаем опиционал из data
            guard let data = data else { return }
            
            do {
                let westWorld = try JSONDecoder().decode(WestWorld.self, from: data)
                //Выходим в основной поток
                DispatchQueue.main.async {
                    completon(westWorld)
                }
            } catch let error {
                print(error)
            }
        }.resume()
    }
}
