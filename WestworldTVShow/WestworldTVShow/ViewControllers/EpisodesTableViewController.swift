//
//  EpisodesTableViewController.swift
//  WestworldTVShow
//
//  Created by Лидия Некрасова on 19.05.2023.
//

import UIKit

class EpisodesTableViewController: UITableViewController {
    
    // MARK: - Public properties
    var westWorlds: WestWorld!
    
    // MARK: - Private properties
    ///Словарь с сезонами сериала
    private lazy var seasons = Dictionary(grouping: westWorlds.embedded.episodes, by: { $0.season })
    
    // MARK: - TableViewDataSource
    override func numberOfSections(in tableView: UITableView) -> Int {
        return seasons.count
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Season: \(seasons.keys.sorted()[section])"
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let season = seasons[section + 1]?.count else { return 0 }
        return season
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "tableViewCell", for: indexPath) as? EpisodeTableViewCell else { return UITableViewCell() }
        
        guard let episode = seasons[indexPath.section + 1]?[indexPath.row] else { return UITableViewCell() }
        
        cell.configure(with: episode)
        return cell
    }
    
    // MARK: - Table view delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let episode = seasons[indexPath.section + 1]?[indexPath.row]
        performSegue(withIdentifier: "descriptionSegue", sender: episode)
    }

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let episodeDetailsVC = segue.destination as? DetailsEpisodeViewController else { return }
            episodeDetailsVC.episode = sender as? Episode
    }
}
