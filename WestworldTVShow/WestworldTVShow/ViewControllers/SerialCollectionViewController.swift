//
//  SerialCollectionViewController.swift
//  WestworldTVShow
//
//  Created by Лидия Некрасова on 18.05.2023.
//

import UIKit

class SerialCollectionViewController: UICollectionViewController {
    
    // MARK: - Private properties
    ///Количество рядов
    private let itemsPerRow: CGFloat = 1
    
    ///Отступы ячейки
    private let sectionInserts = UIEdgeInsets(top: 20,
                                              left: 20,
                                              bottom: 20,
                                              right: 20)
    ///Модель сериалов
    private var westWorlds: [WestWorld] = []
    
    // MARK: - Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchData(from: URLS.westWorldApi.rawValue)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        ///Проверка соединения с интернетом
        ///Если интернет есть, выводим сообщение на консоль. Если нет, выводим allert с ошибкой.
        if Reachability.isConnectedToNetwork() {
            print("Connected")
        } else {
            let allertController = UIAlertController(title: "No Internet Connection",
                                                     message: "Please, check your Internet connection settings",
                                                     preferredStyle: .alert)
            let okButton = UIAlertAction(title: "OK", style: .default, handler: nil)
            
            allertController.addAction(okButton)
            
            present(allertController, animated: true, completion: nil)
        }
    }
    
    // MARK: - UICollectionViewDataSource
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        westWorlds.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "serialCell", for: indexPath) as? SerialCollectionViewCell else { return UICollectionViewCell() }
        
        cell.layer.cornerRadius = 20
        
        let westWorld = westWorlds[indexPath.item]
        cell.configure(with: westWorld)
        
        return cell
    }
    
    // MARK: - UICollectionViewDelegate
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        let westWorld = westWorlds[indexPath.item]
        performSegue(withIdentifier: "seriesSegue", sender: westWorld)
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let episodeVC = segue.destination as? EpisodesTableViewController else { return }
        episodeVC.westWorlds = sender as? WestWorld
    }
    
    // MARK: - Private methods
    private func fetchData(from url: String?) {
        NetworkManager.shared.fetchData(from: url) { westWorld in
            self.westWorlds = [westWorld]
            self.collectionView.reloadData()
        }
    }
}

     // MARK: - UICollectionViewDelegateFlowLayout
extension SerialCollectionViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddingWith = sectionInserts.left * (itemsPerRow + 1)
        let avaliableWidth = collectionView.frame.width - paddingWith
        let widthPerItem = avaliableWidth / itemsPerRow
        return CGSize(width: widthPerItem / 2,
                      height: widthPerItem)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInserts
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInserts.left
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInserts.left
    }
}
