//
//  DetailsEpisodeViewController.swift
//  WestworldTVShow
//
//  Created by Лидия Некрасова on 22.05.2023.
//

import UIKit

class DetailsEpisodeViewController: UIViewController {
    
    // MARK: - Public properties
    var episode: Episode!
    
    // MARK: - IBOutlets
    @IBOutlet weak var epidodeDescriptionLabel: UILabel!
    @IBOutlet weak var episodSummaryLabel: UILabel!
    @IBOutlet weak var episodeImage: ImagesImageView!
    
    // MARK: - Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = episode.name
        epidodeDescriptionLabel.text = episode.description
        episodSummaryLabel.text = episode.summary.replacingOccurrences(of: "<[^>]+>",
                                                                       with: "",
                                                                       options: String.CompareOptions.regularExpression,
                                                                       range: nil)
        episodeImage.fetchImage(from: episode.image.original ?? "")
    }
}
