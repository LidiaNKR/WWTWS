//
//  WestWorld..swift
//  WestworldTVShow
//
//  Created by Лидия Некрасова on 18.05.2023.
//

///Модель сериала
struct WestWorld: Decodable {
    let name: String
    let language: String
    let genres: [String]
    let premiered: String
    let rating: Rating
    let imageSerial: ImageSerial
    let embedded: Embedded
    
    ///Описание сериала
    var description: String {
        
        ///Извлкаем из строки с датой премьеры год выпуска сериала
        let start = premiered.index(premiered.startIndex, offsetBy: 0)
        let end = premiered.index(premiered.startIndex, offsetBy: 4)
        let range = start..<end
        
        return """
               \(premiered[range])
               \(language) · \(genres[0]), \(genres[1]), \(genres[2])
               """
    }
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case language = "language"
        case genres = "genres"
        case rating = "rating"
        case premiered = "premiered"
        case imageSerial = "image"
        case embedded = "_embedded"
    }
}

///Модель рейтинга сериала
struct Rating: Decodable {
    let average: Double
}

///Модель картинки серала
struct ImageSerial: Decodable {
    let medium: String?
}

///Модель массива эпизодов
struct Embedded: Decodable {
    let episodes: [Episode]
}

///Модель эпизода
struct Episode: Decodable {
    let name: String
    let season: Int
    let number: Int
    let airdate: String
    let image: ImageSerie
    let summary: String
    
    ///Описание эпизода
    var description: String {
        """
        Season: \(season), Episode: \(number)
        Date: \(airdate)
        """
    }
}

///Модель картинки эпизода
struct ImageSerie: Decodable {
    let medium: String?
    let original: String?
}

///Ссылка на JSON
enum URLS: String {
    case westWorldApi = "http://api.tvmaze.com/singlesearch/shows?q=westworld&embed=episodes"
}

